
/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package poc;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import poc.entity.CEntity;
import poc.repository.CRepository;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.*;

@SpringBootTest
public class RepositoryTest {

	@Autowired
	DataInit init;

	@Autowired
	CRepository repository;

	@Autowired
	EntityManager em;

	@Test
	void testInitialization() {
		assertThat(repository.count()).isEqualTo(2);
	}


	@Test
	void testRepository() {

		Integer id = repository.findAll().get(0).getId();
		System.out.println("===========================================================================");
		CEntity byId = repository.getById(id);
		System.out.println("...........................................................................");
		byId.getdList().forEach(System.out::println);
		System.out.println("===========================================================================");
	}

	@Test
	void testEntityManagerFind() {

		Integer id = repository.findAll().get(0).getId();
		System.out.println("===========================================================================");

		Map<String, Object> properties = createEntityGraph();

		CEntity byId = em.find(CEntity.class, id, properties);
		System.out.println("...........................................................................");
		byId.getdList().forEach(System.out::println);
		System.out.println("===========================================================================");
	}

	@Test
	void testEntityManagerJpqlQuery() {

		Integer id = repository.findAll().get(0).getId();
		System.out.println("===========================================================================");

		Map<String, Object> properties = createEntityGraph();

		TypedQuery<CEntity> query = em.createQuery("select generatedAlias0 from CEntity as generatedAlias0 where generatedAlias0.id=:param0", CEntity.class);
		query.setParameter("param0", id);
		properties.forEach(query::setHint);

		CEntity byId = query.getResultList().get(0);

		System.out.println("...........................................................................");
		byId.getdList().forEach(System.out::println);
		System.out.println("===========================================================================");
	}

	@Test
	void testEntityManagerCriteriaQuery() {

		Integer id = repository.findAll().get(0).getId();
		System.out.println("===========================================================================");

		Map<String, Object> properties = createEntityGraph();

		CriteriaBuilder cb = em.getCriteriaBuilder();

		CriteriaQuery<CEntity> criteriaQuery = cb.createQuery(CEntity.class);
		Root<CEntity> root = criteriaQuery.from(CEntity.class);
		criteriaQuery.where(cb.equal(root.get("id"), cb.parameter(Integer.class, "id")));

		TypedQuery<CEntity> query = em.createQuery(criteriaQuery);

		query.setParameter("id", id);

		properties.forEach(query::setHint);

		CEntity byId = query.getSingleResult();

		System.out.println("...........................................................................");
		byId.getdList().forEach(System.out::println);
		System.out.println("===========================================================================");
	}


	private Map<String, Object> createEntityGraph() {

		EntityGraph<CEntity> cGraph = em.createEntityGraph(CEntity.class);

		cGraph.addAttributeNodes("a", "b", "dList");

		Map<String, Object> properties = new HashMap<>();
		properties.put("javax.persistence.fetchgraph", cGraph);
		return properties;
	}
}
