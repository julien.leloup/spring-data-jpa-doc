package poc.service;

import org.springframework.transaction.annotation.Transactional;
import poc.entity.CEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import poc.repository.*;

@Component
public class MyService {

    @Autowired
    ARepository aRepository;

    @Autowired
    BRepository bRepository;

    @Autowired
    CRepository cRepository;

    @Autowired
    DRepository dRepository;

    @Autowired
    ERepository eRepository;

    @Transactional
    public CEntity fonctionnementpardefauttexte(Integer id) {
        return cRepository.findById(id);
    }

    @Transactional
    public CEntity entityGraph(Integer id) {
        return  cRepository.getById(id);
    }

    @Transactional
    public CEntity fonctionnementpardefautjpql(Integer id) {
        return cRepository.fonctionnementpardefautjpql(id);
    }

    @Transactional
    public CEntity joinjpql(Integer id) {
        return cRepository.joinjpql(id);
    }

    @Transactional
    public CEntity fetchjpql(Integer id) {
        return cRepository.fetchjpql(id);
    }
}
