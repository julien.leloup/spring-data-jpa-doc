package poc.repository;

import poc.entity.EEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ERepository extends JpaRepository<EEntity, Long> {
}
