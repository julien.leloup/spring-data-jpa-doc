package poc.repository;

import poc.entity.DEntity;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DRepository extends JpaRepository<DEntity, Long> {

    // temp
    @EntityGraph(attributePaths = {"e"})
    List<DEntity> getById(Integer id);
}
