package poc.repository;

import poc.entity.BEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BRepository extends JpaRepository<BEntity, Long> {
}
