package poc.repository;

import poc.entity.CEntity;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CRepository extends JpaRepository<CEntity, Long> {

    // fonctionnementpardefauttexte
    CEntity findById(Integer id);

    // entityGraph
    @EntityGraph(attributePaths = {"a", "b", "dList", "dList.e"})
    CEntity getById(Integer id);

    // fonctionnementpardefautjpql
    @Query(value = "SELECT c FROM CEntity c WHERE c.id = :id")
    CEntity fonctionnementpardefautjpql(Integer id);

    // joinjpql
    @Query(value = "SELECT c FROM CEntity c LEFT JOIN c.a LEFT JOIN c.b LEFT JOIN c.dList resd LEFT JOIN resd.e WHERE c.id = :id")
    CEntity joinjpql(Integer id);

    // fetchjpql
    @Query(value = "SELECT c FROM CEntity c LEFT JOIN FETCH c.a LEFT JOIN FETCH c.b LEFT JOIN FETCH c.dList resd LEFT JOIN FETCH resd.e WHERE c.id = :id")
    CEntity fetchjpql(Integer id);
}
