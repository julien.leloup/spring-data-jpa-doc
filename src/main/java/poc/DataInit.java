package poc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import poc.entity.*;
import poc.repository.*;

@Component
public class DataInit implements ApplicationRunner {

    @Autowired
    ARepository aRepository;

    @Autowired
    BRepository bRepository;

    @Autowired
    CRepository cRepository;

    @Autowired
    DRepository dRepository;

    @Autowired
    ERepository eRepository;

    @Override
    public void run(ApplicationArguments args) {

        // A -----------------------------
        AEntity a1 = new AEntity();
        a1.setId(1);
        a1.setLabel("A1");

        AEntity a2 = new AEntity();
        a2.setId(2);
        a2.setLabel("A2");

        aRepository.save(a1);
        aRepository.save(a2);
        aRepository.flush();



        // B -----------------------------
        BEntity b1 = new BEntity();
        b1.setId(1);
        b1.setLabel("B1");

        BEntity b2 = new BEntity();
        b2.setId(2);
        b2.setLabel("B2");

        bRepository.save(b1);
        bRepository.save(b2);
        bRepository.flush();



        // E -----------------------------
        EEntity e1 = new EEntity();
        e1.setId(1);
        e1.setLabel("E1");

        EEntity e2 = new EEntity();
        e2.setId(2);
        e2.setLabel("E2");

        EEntity e3 = new EEntity();
        e3.setId(3);
        e3.setLabel("E3");

        EEntity e4 = new EEntity();
        e4.setId(4);
        e4.setLabel("E4");

        eRepository.save(e1);
        eRepository.save(e2);
        eRepository.save(e3);
        eRepository.save(e4);
        eRepository.flush();

        // C et D ------------------------
        DEntity d1 = new DEntity();
        d1.setId(1);
        d1.setLabel("D1");
        d1.setE(e1);

        DEntity d2 = new DEntity();
        d2.setId(2);
        d2.setLabel("D2");
        d2.setE(e2);

        CEntity c1 = new CEntity();
        c1.setId(1);
        c1.setLabel("C1");
        c1.setA(a1);
        c1.setB(b1);
        c1.addD(d1);
        c1.addD(d2);

        cRepository.save(c1);



        DEntity d3 = new DEntity();
        d3.setId(3);
        d3.setLabel("D3");
        d3.setE(e3);

        DEntity d4 = new DEntity();
        d4.setId(4);
        d4.setLabel("D4");
        d4.setE(e4);

        CEntity c2 = new CEntity();
        c2.setId(2);
        c2.setLabel("C2");
        c2.setA(a2);
        c2.setB(b2);
        c2.addD(d3);
        c2.addD(d4);

        cRepository.save(c2);

        int id = 5;
        for (int i = 0; i < 10; i++) {
            DEntity dn = new DEntity();
            dn.setId(id++);
            dn.setLabel("label");
            dn.setE(e3);
            dRepository.save(dn);
        }





    }
}
