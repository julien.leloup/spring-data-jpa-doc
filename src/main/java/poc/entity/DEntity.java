package poc.entity;

import javax.persistence.*;

@Entity
@Table(name = "D")
public class DEntity {

    @Id
    private Integer id;

    @Column
    private String label;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "C_ID")
    private CEntity c;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "E_ID")
    private EEntity e;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public CEntity getC() {
        return c;
    }

    public void setC(CEntity c) {
        this.c = c;
    }

    public EEntity getE() {
        return e;
    }

    public void setE(EEntity e) {
        this.e = e;
    }
}
