package poc.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "C")
public class CEntity {

    @Id
    private Integer id;

    @Column
    private String label;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "A_ID")
    private AEntity a;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "B_ID")
    private BEntity b;

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "c",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<DEntity> dList = new ArrayList<>();

    public void addD(DEntity d) {
        dList.add(d);
        d.setC(this);
    }

    public void removeD(DEntity d) {
        dList.remove(d);
        d.setC(null);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public AEntity getA() {
        return a;
    }

    public void setA(AEntity a) {
        this.a = a;
    }

    public BEntity getB() {
        return b;
    }

    public void setB(BEntity b) {
        this.b = b;
    }

    public List<DEntity> getdList() {
        return dList;
    }

    public void setdList(List<DEntity> dList) {
        this.dList = dList;
    }
}
