package poc.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "E")
public class EEntity {

    @Id
    private Integer id;

    @Column
    private String label;

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "e",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<DEntity> dList = new ArrayList<>();

    public void addD(DEntity d) {
        dList.add(d);
        d.setE(this);
    }

    public void removeD(DEntity d) {
        dList.remove(d);
        d.setE(null);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<DEntity> getdList() {
        return dList;
    }

    public void setdList(List<DEntity> dList) {
        this.dList = dList;
    }
}
