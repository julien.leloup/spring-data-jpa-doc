package poc.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "A")
public class AEntity {

    @Id
    private Integer id;

    @Column
    private String label;

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "a",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<CEntity> cList = new ArrayList<>();

    public void addC(CEntity c) {
        cList.add(c);
        c.setA(this);
    }

    public void removeC(CEntity c) {
        cList.remove(c);
        c.setA(null);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<CEntity> getcList() {
        return cList;
    }

    public void setcList(List<CEntity> cList) {
        this.cList = cList;
    }
}
