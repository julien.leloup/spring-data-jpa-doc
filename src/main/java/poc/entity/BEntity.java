package poc.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "B")
public class BEntity {

    @Id
    private Integer id;

    @Column
    private String label;

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "b",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<CEntity> cList = new ArrayList<>();

    public void addC(CEntity c) {
        cList.add(c);
        c.setB(this);
    }

    public void removeC(CEntity c) {
        cList.remove(c);
        c.setB(null);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<CEntity> getcList() {
        return cList;
    }

    public void setcList(List<CEntity> cList) {
        this.cList = cList;
    }
}
